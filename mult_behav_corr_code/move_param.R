update_move_param <- function(curr_move_params, behav_params, refined_path) {

  # check that the current movement parameters have the correct length
  if (length(curr_move_params) != num_move_params) stop('Incorrect number of parameters')

  # propose the new movement parameters, and evaluate the proposal likelihood ratio (log densities)
  pert_sd <- sqrt(pert_var)  
  lower <- rep(0, num_move_params)
  upper <- rep(Inf, num_move_params)
  prop_move_params <- rtnorm(rep(1, num_move_params), mean = curr_move_params, sd = pert_sd, lower = lower, upper = upper)
  prop_given_curr_lik <- sum(dtnorm(prop_move_params, mean = curr_move_params, sd = pert_sd, lower = lower, upper = upper, log = TRUE))
  curr_given_prop_lik <- sum(dtnorm(curr_move_params, mean = prop_move_params, sd = pert_sd, lower = lower, upper = upper, log = TRUE))
  
  # calculate the log likelihood of the prior distribution given current and proposed params           
  prop_prior_lik <- calc_prior_lik(move_params = prop_move_params)
  curr_prior_lik <- calc_prior_lik(move_params = curr_move_params)

  # calculate the log likelihood of the refined path, given current and proposed params 
  prop_refined_path_lik <- calc_refined_path_lik(refined_path = refined_path, move_params = prop_move_params, behav_params = behav_params)
  curr_refined_path_lik <- calc_refined_path_lik(refined_path = refined_path, move_params = curr_move_params, behav_params = behav_params)

  # calculate the standard MH acceptance ratio, taking exponential as all likelihoods are log likelihoods
  accept_prob <- exp(curr_given_prop_lik - prop_given_curr_lik + 
                     prop_prior_lik + prop_refined_path_lik - 
                     curr_prior_lik - curr_refined_path_lik)

  list(accept_prob = accept_prob, prop_move_params = prop_move_params)
}



calc_refined_path_lik <- function(refined_path, move_params, behav_params) {

  # initial bearing has a uniform likelihood between -pi, pi
  initial_bearing_lik <- dunif(refined_path$bearings[1], min = -pi, max = pi, log = TRUE)  

  # likelihood of the rest of the bearings can be gained by a conditional argument
  cond_bearing_dist <- calc_cond_bearing_dist(bearings = refined_path$bearings, times = refined_path$times, behavs = refined_path$behavs, turn_var = move_params[q.bearings])
  # check that there will not be a length mis-match leading to an incorrect recycling of values in the dnorm call
  if (length(refined_path$bearings[-1]) != length(cond_bearing_dist$mean) || length(refined_path$bearings[-1]) != length(cond_bearing_dist$sd)) stop('Conditional bearing length mis-match')
  cond_bearing_lik <- sum(dnorm(refined_path$bearings[-1], mean = cond_bearing_dist$mean, sd = cond_bearing_dist$sd, log=TRUE))
  
  # initial step likelihood is taken from OU equilibrium distribution and behavioural process equilibrium distribution
  equil_step_dist <- calc_equil_step_dist(speed_mean = move_params[q.speed_mean], speed_corr = move_params[q.speed_corr], speed_var = move_params[q.speed_var], step_time = refined_path$times[2] - refined_path$times[1]) 
  steady_state <- calc_markov_steady_state(behav_params = behav_params)
  # check that there will not be a length mis-match leading to an incorrect recycling of values in the dnorm call
  if (length(equil_step_dist$mean) != length(steady_state)) stop('Equilibrium distribution and steady state length mis-match')
  initial_step_lik <- log(sum(dnorm(refined_path$steps[1], mean = equil_step_dist$mean, sd = equil_step_dist$sd) * steady_state))

  # likelihood of the rest of the steps can be gained by a conditional argument
  cond_step_dist <- calc_cond_step_dist(steps = refined_path$steps, times = refined_path$times, behavs = refined_path$behavs, speed_mean = move_params[q.speed_mean], speed_corr = move_params[q.speed_corr], speed_var = move_params[q.speed_var])
  # check that there will not be a length mis-match leading to an incorrect recycling of values in the dnorm call
  if (length(refined_path$steps[-1]) != length(cond_step_dist$mean) || length(refined_path$steps[-1]) != length(cond_step_dist$sd)) stop('Conditional step length mis-match')
  cond_step_lik <- sum(dnorm(refined_path$steps[-1], mean = cond_step_dist$mean, sd = cond_step_dist$sd, log = TRUE)) 

  # all likelihoods are log likelihoods so sum gives the refined path likelihood
  initial_bearing_lik + cond_bearing_lik + initial_step_lik + cond_step_lik
}



calc_markov_steady_state <- function(behav_params) {

  # checks on sizes of the input parameters
  if (length(behav_params$lambda) != num_states) stop('Should have switch rates for each behavioural state')
  if (length(behav_params$q) != num_states^2) stop('Should have switch probs for each behavioural state')
  
  # create the generator matrix Q from the switching rates and probabilities
  Q <- matrix(NA, nrow = num_states, ncol = num_states)
  diag(Q) <- -behav_params$lambda
  for (i in 1:num_states) {
    Q[i, -i] <- behav_params$q[i, -i] * rep(behav_params$lambda[i], num_states - 1)
  }

  # solve the formula to get the stationary distribution
  Q[ , num_states] <- rep(1, times = num_states)
  as.vector(solve(t(Q), matrix(c(rep(0, times = num_states - 1), 1), ncol = 1)))
}



calc_equil_step_dist <- function(speed_mean, speed_corr, speed_var, step_time) {

  # checks of the lengths of the input parameters
  if (length(speed_mean) != num_states || length(speed_corr) != num_states || length(speed_var) != num_states) stop('Should have parameter values for each behavioural state')
  if (length(step_time) != 1) stop('Time step should be single value')

  # OU speed equilibrium distribution, multiplied by time because we want step distribution 
  mean <- step_time * speed_mean
  sd <- step_time * sqrt(speed_var / (2 * speed_corr))

  list(mean = mean, sd = sd)
}



calc_cond_step_dist <- function(steps, times, behavs, speed_mean, speed_corr, speed_var) {

  # checks on the sizes of the input parameters
  if (length(steps) != length(behavs)) stop('Step and behaviour vectors not of equal length')
  if (length(times) != length(steps) + 1) stop('Times vector should have one more element that steps')
  if (length(speed_mean) != num_states || length(speed_corr) != num_states || length(speed_var) != num_states) stop('Should have parameter values for each behavioural state')

  # get the time intervals of each step and the parameters for each step using the behaviour at each point
  time_diffs <- diff(times)
  speed_mean_vec <- speed_mean[behavs[-length(behavs)]]
  speed_corr_vec <- speed_corr[behavs[-length(behavs)]]
  speed_var_vec <- speed_var[behavs[-length(behavs)]]

  # mean and variance are the conditional OU process, multiplied by time difference because we want steps not speeds
  step_mean <- time_diffs[-1] * (speed_mean_vec + exp(-speed_corr_vec * time_diffs[-length(time_diffs)]) * (steps[-length(steps)] / time_diffs[-length(time_diffs)] - speed_mean_vec))
  step_sd <- time_diffs[-1] * sqrt(speed_var_vec / (2 * speed_corr_vec) * (1 - exp(-2 * speed_corr_vec * time_diffs[-length(time_diffs)]))) 
    
  list(mean = step_mean, sd = step_sd)    
}



calc_cond_bearing_dist <- function(bearings, times, behavs, turn_var) {

  # some exception checks on the sizes of the input parameters
  if (length(bearings) != length(behavs)) stop('Bearing and behaviour vectors not of equal length')
  if (length(times) != length(bearings) + 1) stop('Time vector should contain one more element than bearings')
  if (length(turn_var) != num_states ) stop('Should have a turn variance parameter for each behavioural state')

  mean <- bearings[-length(bearings)]
  sd <- sqrt(turn_var[behavs[-length(bearings)]] * diff(times[-length(times)]))

  list(mean = mean, sd = sd)
}