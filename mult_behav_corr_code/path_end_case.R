update_refined_path_end <- function(fixed_values, curr_values, move_params, behav_params) {

  # propose a new section of behaviours by simulating a markov process forwards in time
  prop_behav <- sim_markov_forward(fixed_times = fixed_values$inc_times, fixed_state = fixed_values$inc_behav, markov_param = behav_params)

  # using the new behaviours, create the refined time scale for this section 
  prop_times <- create_refined_times(behav_proc_times = prop_behav$times, contained_obs_times = fixed_values$obs_times)
  if (length(prop_times$refined_times) < 3) return(list(behav_fail = 1))
  # and a refined set of behaviours, as this will be more useful than switching times
  prop_behav_refined <- create_refined_behavs(behav_proc = prop_behav, refined_times = prop_times$refined_times)

  # propose a new section of bearings by simulating brownian motion forwards in time
  prop_bearings <- sim_brownian_forward(fixed_time = fixed_values$time, fixed_behav = fixed_values$behav, fixed_value = fixed_values$bearing, 
                                        times = prop_times$refined_times, behavs = prop_behav_refined, 
                                        volatility = move_params[q.bearings])	  

  # construct the distribution of the steps on a (time multiplied) OU process (for both the new and the old as they have different times and behaviours)
  prop_step_dist <- calc_dist_step_forward(fixed_time = fixed_values$time, after_end_time = fixed_values$after_time, fixed_behav = fixed_values$behav, fixed_value = fixed_values$step, 
                                           times = prop_times$refined_times, behavs = prop_behav_refined, 
                                           mean_para = move_params[q.speed_mean], corr_para = move_params[q.speed_corr], var_para = move_params[q.speed_var])
  curr_step_dist <- calc_dist_step_forward(fixed_time = fixed_values$time, after_end_time = fixed_values$after_time, fixed_behav = fixed_values$behav, fixed_value = fixed_values$step, 
                                           times = curr_values$times, behavs = curr_values$behavs, 
                                           mean_para = move_params[q.speed_mean], corr_para = move_params[q.speed_corr], var_para = move_params[q.speed_var])

  # construct the distribution of the fixed locations, given the bearings (for both the new and the old)
  prop_fixed_locs_dist <- calc_dist_fixed_locs_end(start_loc = fixed_values$start_loc, fixed_index = prop_times$loc_index,
                                                   bearings = prop_bearings, step_mean = prop_step_dist$mean, step_covar = prop_step_dist$covar)
  curr_fixed_locs_dist <- calc_dist_fixed_locs_end(start_loc = fixed_values$start_loc, fixed_index = curr_values$loc_index,
                                                   bearings = curr_values$bearings, step_mean = curr_step_dist$mean, step_covar = curr_step_dist$covar)

  # construct the distribution of the steps, conditional on the bearings and fixed locations 
  prop_cond_step_dist <- calc_dist_cond_steps(step_mean = prop_step_dist$mean, step_covar = prop_step_dist$covar, 
                                              loc_mean = prop_fixed_locs_dist$loc_mean, loc_covar = prop_fixed_locs_dist$loc_covar, loc_step_covar = prop_fixed_locs_dist$loc_step_covar, 
                                              fixed_locs = fixed_values$locs)

  # propose a new section of steps by drawing from the conditioned distribution, but must be done using SVD 
  prop_steps <- sim_singular_normal(covar = prop_cond_step_dist$covar, mean = prop_cond_step_dist$mean)

  # acceptance probability for the proposed path over the current
  accept_prob <- exp(dmvn(fixed_values$locs, mu = prop_fixed_locs_dist$loc_mean, sigma = prop_fixed_locs_dist$loc_covar, log = TRUE) - 
                     dmvn(fixed_values$locs, mu = curr_fixed_locs_dist$loc_mean, sigma = curr_fixed_locs_dist$loc_covar, log = TRUE))

  list(behav_fail = 0, accept_prob = accept_prob, 
       prop_times = prop_times$refined_times, prop_behavs = prop_behav_refined, prop_bearings = prop_bearings, prop_steps = prop_steps)
}



sim_markov_forward <- function(fixed_times, fixed_state, markov_param) {
  
  # set up values
  switch_times <- curr_time <- fixed_times[1]
  switch_states <- curr_state <- fixed_state
  
  repeat {
    
    # find out when the time of the next switch will occur
    curr_time <- curr_time + rexp(1, rate = markov_param$lambda[curr_state])
    
    # if this is past the end time, then we stop and do not consider it
    if (curr_time > fixed_times[2]) break

    # otherwise we choose what state we are switching into 
    if (num_states == 2) {
      curr_state <- (1:num_states)[-curr_state]
    } else {
      curr_state <- sample((1:num_states)[-curr_state], size = 1, prob = markov_param$q[curr_state , -curr_state])
    }

    # then add this information to a running vector of switches
    switch_times <- c(switch_times, curr_time)
    switch_states <- c(switch_states, curr_state)
    }

    list(times = c(switch_times, fixed_times[2]), states = c(switch_states, curr_state))
}



sim_brownian_forward <- function(fixed_time, fixed_behav, fixed_value, times, behavs, volatility) {
    
  # checks on the lengths of the input variables  
  if (length(volatility) != num_states) stop('Should have a turn volatility for each behavioural state')
  if (length(behavs) != length(times)) stop('Behav and time length mis-match')
        
  # transform the times, so that the starting time is 0 and the process has a constant volatility of 1 (includes the end time in this transformation)      
  transformed_times <- cumsum(c(0, volatility[c(fixed_behav, behavs[-length(behavs)])] * diff(c(fixed_time, times))))        
  
  # draw brownian motion simulation
  fixed_value + cumsum(sqrt(diff(transformed_times)) * rnorm(length(behavs)))
}



set_fixed_values_end <- function(start, end, refined_path, obs, contained_obs) {
  list(inc_behav = refined_path$behavs[start],
       inc_times = refined_path$times[c(start, end)],
       obs_times = obs$times[contained_obs],
       time = refined_path$times[start - 1],
       behav = refined_path$behavs[start - 1],
       bearing = refined_path$bearings[start - 1],
       step = refined_path$steps[start - 1],
       after_time = refined_path$times[end + 1],
       locs = as.vector(rbind(c(obs$x[contained_obs], obs$x[length(obs$x)]), c(obs$y[contained_obs], obs$y[length(obs$y)]))),
       start_loc = c(refined_path$X[start], refined_path$Y[start])
       )  
} 



set_current_values_end <- function(start, end, refined_path, contained_obs_index) {
  list(times = refined_path$times[start:end],
       behavs = refined_path$behavs[start:end],
       bearings = refined_path$bearings[start:end],
       loc_index = c(contained_obs_index - start, end - start + 1)
      ) 
}



calc_dist_step_forward <- function(fixed_time, after_end_time, fixed_behav, fixed_value, times, behavs, mean_para, corr_para, var_para) {

  # convert the fixed value steps to speeds 
  fixed_speed <- fixed_value / (times[1] - fixed_time)

  # call the OU bridge function with the fixed_speeds 
  speed_dist <- calc_dist_OU_forward(fixed_time = fixed_time, fixed_behav = fixed_behav, fixed_value = fixed_speed, 
                                     times = times, behavs = behavs, 
                                     mean_para = mean_para, corr_para = corr_para, var_para = var_para)

  # convert the speed bridge to a step bridge 
  time_diffs <- diff(c(times, after_end_time))
  step_mean <- speed_dist$mean * time_diffs
  step_cov <- diag(time_diffs) %*% speed_dist$covar %*% diag(time_diffs)

  list(mean = step_mean, covar = step_cov)
}



calc_dist_OU_forward <- function(fixed_time, fixed_behav, fixed_value, times, behavs, mean_para, corr_para, var_para) {
  
  # set up things
  num_points <- length(times)
  OU_mean <- rep(NA, times = num_points + 1)
  OU_covar <- matrix(NA, nrow = num_points + 1, ncol = num_points + 1)
  time_diffs <- diff(c(fixed_time, times))
  all_behavs <- c(fixed_behav, behavs)

  # initialise start values (which are the known fixed values, hence 0 variance)    
  OU_mean[1] <- fixed_value
  OU_covar[1, ] <- OU_covar[ , 1] <- 0

  # iterate through, filling in the mean and covariance of each entry based on the last, following an OU process 
  for(j in 2:(num_points + 1)) {
  
    # set current params, use the behaviour from the previous time and the time difference between now and the previous point
    c_behav <- all_behavs[j - 1]
    c_corr_para <- corr_para[c_behav]
    expon_t <- exp(-c_corr_para * time_diffs[j - 1])

    # iterative mean and variance, standard formula being followed
    OU_mean[j] <- OU_mean[j - 1] * expon_t + mean_para[c_behav] * (1 - expon_t)
    OU_covar[j, j] <- OU_covar[j - 1, j - 1] * expon_t^2 + var_para[c_behav] * (1 - expon_t^2) / (2 * c_corr_para)     

    # if we are filling in the final point, then there is no covariance row/column bits to fill in, so we break from the loop here
    if (j == (num_points + 1)) break
    # otherwise, we fill in the row/column variances to the right/below the variance that has just been calculated
    for(k in (j + 1):(num_points + 1)) {
      # iterative covariances, again following a formula
      OU_covar[j, k] <- OU_covar[k, j] <- OU_covar[j, k - 1] * exp(-corr_para[all_behavs[k - 1]] * time_diffs[k - 1])
    }
  }

  list(mean = OU_mean[-1], covar = OU_covar[-1, -1])  
}



calc_dist_fixed_locs_end <- function(start_loc, fixed_index, bearings, step_mean, step_covar) {
  
  # set up storage 
  num_points <- length(fixed_index)
  loc_mean <- rep(NA, times = 2 * num_points)
  loc_covar <- matrix(NA, ncol = 2 * num_points, nrow = 2 * num_points)
  loc_step_covar <- matrix(NA, ncol = 2 * num_points, nrow = length(step_mean))

  # loop through the number of fixed location points
  for (i in 1:num_points) {
  
    # the mean x and y location  
    loc_mean[2 * i - 1] <- start_loc[1] + sum(step_mean[1:fixed_index[i]] * cos(bearings[1:fixed_index[i]]))
    loc_mean[2 * i] <- start_loc[2] + sum(step_mean[1:fixed_index[i]] * sin(bearings[1:fixed_index[i]]))
  
    # loop through the number of fixed location points to fill in the whole row of the covariance matrix
    for (j in 1:num_points) {

      part_of_step_covar <- step_covar[1:fixed_index[i], 1:fixed_index[j]]
      # 2 by 2 block of covariances between the i,j^th locations (because a location is x and y so end up with 2x2)
      loc_covar[(2 * i - 1):(2 * i), (2 * j - 1):(2 * j)] <- matrix(c(sum(part_of_step_covar * outer(1:fixed_index[i], 1:fixed_index[j], FUN = function(X, Y) cos(bearings[X]) * cos(bearings[Y]))),
                                                                      sum(part_of_step_covar * outer(1:fixed_index[i], 1:fixed_index[j], FUN = function(X, Y) sin(bearings[X]) * cos(bearings[Y]))),
                                                                      sum(part_of_step_covar * outer(1:fixed_index[i], 1:fixed_index[j], FUN = function(X, Y) cos(bearings[X]) * sin(bearings[Y]))),
                                                                      sum(part_of_step_covar * outer(1:fixed_index[i], 1:fixed_index[j], FUN = function(X, Y) sin(bearings[X]) * sin(bearings[Y])))),
                                                                    nrow = 2, ncol = 2)
    }

    # loop through the number of steps on the section to fill in the whole row of the loc-step covariance matrix
    for (k in 1:length(step_mean)) {

      # 1 by 2 block of covariances for the k^th step and the i^th location
      loc_step_covar[k, (2 * i - 1):(2 * i)] <- c(sum(cos(bearings[1:fixed_index[i]]) * step_covar[k, 1:fixed_index[i]]),
                                                  sum(sin(bearings[1:fixed_index[i]]) * step_covar[k, 1:fixed_index[i]]))
    }
  }  

  list(loc_mean = loc_mean, loc_covar = loc_covar, loc_step_covar = loc_step_covar)   
}